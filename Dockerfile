FROM node:16-alpine AS BUILD

WORKDIR "/app"

# Install app dependencies
COPY . /app/
RUN npm install
RUN npm run build
RUN npm prune --production

FROM node:16-alpine

ENV NODE_ENV production
ENV PORT 3000
ENV CACHE_FOLDER /var/hydrant/cache
ENV DATABASE_FOLDER /var/hydrant/database
ENV MEDIA_FOLDER /var/hydrant/media
ENV TMP_FOLDER /var/hydrant/tmp
ENV INTERNAL_URL http://localhost:3000

RUN mkdir -p /var/hydrant/{cache,database,media,tmp}

WORKDIR "/app"

# Bundle app source
COPY --from=BUILD /app/node_modules/ node_modules/
COPY --from=BUILD /app/dist/ ./

EXPOSE 3000
CMD ["node", "src/main"]
