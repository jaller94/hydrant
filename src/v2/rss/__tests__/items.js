const fsPromises = require('fs').promises;
const {
    getItems,
} = require('../items');

describe('getItems', () => {
    test('returns an empty object for no commands', async () => {
        const content = await fsPromises.readFile(__dirname + '/tagesschau.rss', 'utf8');
        const actual = getItems(content);
        expect(actual).toHaveLength(2);
        expect(actual).toMatchSnapshot();
    });
});
