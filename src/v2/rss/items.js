'use strict';
const { select } = require('xpath');
const dom = require('@xmldom/xmldom').DOMParser;
const XMLSerializer = require('@xmldom/xmldom').XMLSerializer;

/**
 * Returns all items of a feed as separate XML strings.
 * @param {string} document The entire XML document
 * @returns {string[]}
 */
function getItems(document) {
    const doc = new dom({ errorHandler: console.error }).parseFromString(document);
    const items = select('//rss//channel//item', doc);
    return items.map(obj => XMLSerializer.prototype.serializeToString(obj));
}

module.exports = {
    getItems,
};
