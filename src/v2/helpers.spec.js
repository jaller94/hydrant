const {
    getFilterFunction,
    getTransformationObject,
} = require('./helpers');

describe('getTransformationObject', () => {
    test('returns an empty object for no commands', () => {
        const actual = getTransformationObject({});
        expect(actual).toEqual({
            sources: [],
            filters: {},
        });
    });
    test('returns one source', () => {
        const actual = getTransformationObject({
            i: 'https://tagesschau.de',
        });
        expect(actual).toEqual({
            sources: [
                'https://tagesschau.de',
            ],
            filters: {},
        });
    });
    test('returns three sources', () => {
        const actual = getTransformationObject({
            i: 'https://tagesschau.de',
            i1: 'https://bbc.co.uk',
            i2: 'https://wdr.de',
        });
        expect(actual).toEqual({
            sources: [
                'https://tagesschau.de',
                'https://bbc.co.uk',
                'https://wdr.de',
            ],
            filters: {},
        });
    });
    test('returns a Kb filter', () => {
        const actual = getTransformationObject({
            Kb: 'world',
        });
        expect(actual).toEqual({
            sources: [],
            filters: {
                Kb: 'world',
            },
        });
    });
});

describe('getFilterFunction', () => {
    test('filters by title', () => {
        const matches = [
            {
                id: '1',
                category: 'Stable Updates',
            },
            {
                id: '2',
                category: [
                    'Stable Updates',
                    'Unstable Updates',
                ],
            },
        ];
        const doesNotMatch = [
            {
                id: '10',
                category: 'Unstable Updates',
            },
            {
                id: '11',
            },
            {
                id: '11',
                category: [
                    'Unstable Updates',
                ],
            },
        ];
        const func = getFilterFunction('K', 'b', 'Stable Updates');
        const actual = [...matches, ...doesNotMatch].filter(func);
        expect(actual).toEqual(matches);
    });
});
