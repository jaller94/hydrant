const fsPromises = require('fs').promises;
const express = require('express');
const superagent = require('superagent');

const BASE_URL = process.env.BASE_URL || 'http://localhost:3000';

describe('v2 API', () => {
    let app, feed, server;
    beforeAll(async () => {
        feed = await fsPromises.readFile(__dirname + '/feed.rss', 'utf8');
        app = express();
        app.get('/', (req, res) => {
            res.send(feed);
        });
        server = app.listen(3001); 
    });
    afterAll(() => {
        server.close();
    });
    test('returns the feed', async () => {
        const response = await superagent.get(`${BASE_URL}/v2?i=${encodeURIComponent('http://localhost:3001/')}&Ts=&Te=&Tc=&TC=&Kb=`);
        expect(response.ok).toBe(true);
        expect(response.body.toString()).toMatch(/^<\?xml version=/);
    });
    test('returns only items containing Polizei', async () => {
        const response = await superagent.get(`${BASE_URL}/v2?i=${encodeURIComponent('http://localhost:3001/')}&Ts=&Te=&Tc=Polizei&TC=&Kb=`);
        expect(response.ok).toBe(true);
        expect(response.body.toString()).toContain('polizeieinsatz-stuttgart-102');
        expect(response.body.toString()).not.toContain('einreiseregeln-moselle-101');
    });
    test('returns only items starting with Virus', async () => {
        const response = await superagent.get(`${BASE_URL}/v2?i=${encodeURIComponent('http://localhost:3001/')}&Ts=Virus&Te=&Tc=&TC=&Kb=`);
        expect(response.ok).toBe(true);
        expect(response.body.toString()).not.toContain('polizeieinsatz-stuttgart-102');
        expect(response.body.toString()).toContain('einreiseregeln-moselle-101');
    });
});
