'use strict';
const express = require('express');
const router = express.Router();
const { create } = require('xmlbuilder2');

const {
    filterItems,
    fixXmlbuilderObjectBug,
    getCanonicalUrl,
    getChannelfromXMLObject,
    getInput,
    getTransformationObject,
    removeXMLStyle,
} = require('./helpers');

async function processTransformation(obj) {
    if (obj.sources.length === 0) {
        return {
            message: 'No sources defined.',
            status: 400,
        };
    }

    if (obj.sources.length > 9) {
        console.log(`Denied a request with ${obj.sources.length} sources.`);
        throw {
            message: 'Please don\'t request more than 9 sources.',
            status: 400,
        };
    }

    let inputs;
    try {
        inputs = await Promise.all(obj.sources.map(source => getInput(source)));
    } catch (error) {
        console.log(error);
        throw {
            message: 'Failed to get all sources.',
            status: 400,
        };
    }

    const doc = inputs[0];

    try {
        removeXMLStyle(doc);
    } catch (error) {
        console.log('Failed to remove an XML style.');
        console.log(error);
        throw {
            message: 'Failed to remove an XML style.',
            status: 500,
        };
    }

    const docObject = create(doc).toObject();
    fixXmlbuilderObjectBug(docObject);

    let channel;
    try {
        channel = getChannelfromXMLObject(docObject);
    } catch (error) {
        console.log('Failed to find items of a source.');
        console.log(error);
        throw {
            message: 'Failed to find items of a source.',
            status: 500,
        };
    }

    for (let i = 1; i < inputs.length; i++) {
        const input = inputs[i];
        let channelToBeMerged;
        try {
            channelToBeMerged = getChannelfromXMLObject(input);
        } catch (error) {
            console.log('Failed to find items of a source.');
            console.log(error);
            throw {
                status: 500,
                message: 'Failed to find items of a source.',
            };
        }
        channel.item.concat(channelToBeMerged.item);
    }

    channel.item = filterItems(channel.item, obj.filters);

    channel.item.forEach(item => {
        if (typeof item.title === 'string') {
            item.title = item.title.replace(/&amp;/g, '&');
        }
        if (typeof item.description === 'string') {
            item.description = item.description.replace(/&amp;/g, '&');
        }
    });

    return docObject;
}

router.get('/', async (req, res) => {
    try {
        const obj = getTransformationObject(req.query);    
        const docObject = await processTransformation(obj);
        res.status(200)
            .header('Content-Type', 'application/rss+xml; charset=UTF-8')
            .end(
                create(docObject).end({ prettyPrint: true })
            );
    } catch (error) {
        if (error instanceof Error) {
            console.error(error);
            return res.status(500).end('An unexpected internal error occured.');
        } else {
            return res.status(error.status || 500).end(error.message || 'An error happened: No message.');
        }
    }
});

const MONTHS = 'Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec'.split(',');

router.get('/view', async (req, res) => {
    try {
        const obj = getTransformationObject(req.query);
        const docObject = await processTransformation(obj);
        const channel = getChannelfromXMLObject(docObject);
        const formatDate = (dateString) => {
            const now = new Date();
            const date = new Date(dateString);
            const sameYear = date.getFullYear() === date.getFullYear();
            const sameDay = sameYear && date.getMonth() === now.getMonth() && date.getDate() === now.getDate();
            const month = MONTHS[date.getMonth()];
            if (sameDay) {
                return `${date.getHours().toString().padStart(2, '0')}:${date.getMinutes().toString().padStart(2, '0')}`;
            }
            if (sameYear) {
                return `${date.getDate()} ${month} ${date.getHours().toString().padStart(2, '0')}:${date.getMinutes().toString().padStart(2, '0')}`;
            }
            return `${date.getDate()} ${month}, ${date.getFullYear()}`;
        };
        res.render('preview', {
            feedUrl: getCanonicalUrl(obj),
            formatDate,
            items: channel.item,
        });
    } catch (error) {
        res.render('preview', {
            errorMessage: error.message,
        });
    }
});

module.exports = router;
