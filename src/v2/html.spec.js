const {
    extractFeedUrlsFromHTML,
} = require('./html');

describe('extractFeedUrlsFromHTML', () => {
    test('can deal with a full URL', () => {
        const html = `<!DOCTYPE html>
        <html><head>
            <link rel="alternate" type="application/rss+xml" title="tagesschau.de RSS2-Feed" href="https://www.tagesschau.de/xml/rss2/" />
        </head></html>
        `;
        const actual = extractFeedUrlsFromHTML(html, 'https://www.tagesschau.de/');
        expect(actual).toStrictEqual(['https://www.tagesschau.de/xml/rss2/']);
    });
    test('can deal with an absolute URL', () => {
        const html = `<!DOCTYPE html>
        <html><head>
            <link rel="alternate" type="application/rss+xml" title="tagesschau.de RSS2-Feed" href="/xml/rss2/" />
        </head></html>
        `;
        const actual = extractFeedUrlsFromHTML(html, 'https://www.tagesschau.de/page');
        expect(actual).toStrictEqual(['https://www.tagesschau.de/xml/rss2/']);
    });
    test('can deal with a relative URL when the document\'s path is 0 levels deep', () => {
        const html = `<!DOCTYPE html>
        <html><head>
            <link rel="alternate" type="application/rss+xml" title="tagesschau.de RSS2-Feed" href="xml/rss2/" />
        </head></html>
        `;
        const actual = extractFeedUrlsFromHTML(html, 'https://www.tagesschau.de/page');
        expect(actual).toStrictEqual(['https://www.tagesschau.de/xml/rss2/']);
    });
    test('can deal with a relative URL  when the document\'s path is 1 level deep', () => {
        const html = `<!DOCTYPE html>
        <html><head>
            <link rel="alternate" type="application/rss+xml" title="tagesschau.de RSS2-Feed" href="xml/rss2/" />
        </head></html>
        `;
        const actual = extractFeedUrlsFromHTML(html, 'https://www.tagesschau.de/page/');
        expect(actual).toStrictEqual(['https://www.tagesschau.de/page/xml/rss2/']);
    });
    test('can deal with a relative URL  when the document\'s path is 2 level deep', () => {
        const html = `<!DOCTYPE html>
        <html><head>
            <link rel="alternate" type="application/rss+xml" title="tagesschau.de RSS2-Feed" href="xml/rss2/" />
        </head></html>
        `;
        const actual = extractFeedUrlsFromHTML(html, 'https://www.tagesschau.de/section/page/1');
        expect(actual).toStrictEqual(['https://www.tagesschau.de/section/page/xml/rss2/']);
    });
    test('can deal with a relative URL when the document uses the HTML namespace', () => {
        const html = `<!DOCTYPE html>
        <html xmlns="http://www.w3.org/1999/xhtml" lang="de"><head>
            <link rel="icon" href="/res/assets/image/favicon.ico" type="image/x-icon">
            <meta charset="UTF-8"/>
            <meta name="viewport" content="width=device-width"/>
            <link rel="alternate" type="application/rss+xml" title="tagesschau.de RSS2-Feed" href="xml/rss2/" />
        </head></html>
        `;
        const actual = extractFeedUrlsFromHTML(html, 'https://www.tagesschau.de/section/page/1');
        expect(actual).toStrictEqual(['https://www.tagesschau.de/section/page/xml/rss2/']);
    });
});
