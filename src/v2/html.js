'use strict';
const xpath = require('xpath');
const DOMParser = require('@xmldom/xmldom').DOMParser;

const select = xpath.useNamespaces({ 'html': 'http://www.w3.org/1999/xhtml' });

/**
 * Returns all feed URLs of a HTML document.
 * @param {string} html HTML document
 * @param {string} url URL of the document
 * @returns {string[]}
 */
function extractFeedUrlsFromHTML(html, url) {
    const doc = new DOMParser({ errorHandler: {} }).parseFromString(html);
    const links = [
        ...select('//link[@rel="alternate" and @type="application/rss+xml"]/@href', doc),
        ...select('//link[@rel="alternate" and @type="application/atom+xml"]/@href', doc),
        ...select('//html:link[@rel="alternate" and @type="application/rss+xml"]/@href', doc),
        ...select('//html:link[@rel="alternate" and @type="application/atom+xml"]/@href', doc),
    ];
    return links.map(link => {
        if (/:\/\//.test(link.value)) {
            // The link contains a full URL with a schema.
            return link.value;
        }
        const isAbsolute = /^\//.test(link.value);
        const newUrl = new URL(url);
        newUrl.search = '';
        newUrl.hash = '';
        if (isAbsolute) {
            newUrl.pathname = link.value;
        } else {
            newUrl.pathname = newUrl.pathname.substring(0, newUrl.pathname.lastIndexOf('/') + 1) + link.value;
        }
        return newUrl.toString();
    });
}

module.exports = {
    extractFeedUrlsFromHTML,
};
