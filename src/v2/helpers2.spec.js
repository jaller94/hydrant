const { create } = require('xmlbuilder2');
const {
    fixXmlbuilderObjectBug,
} = require('./helpers');

describe('fixXmlbuilderObjectBug', () => {
    test('correctly transforms XML to an object', () => {
        const doc = create('<?xml version="1.0"?><title>Hello</title>').toObject();
        fixXmlbuilderObjectBug(doc);
        expect(doc).toEqual({
            title: 'Hello',
        });
    });
    test('correctly transforms XML with HTML content to an object', () => {
        const doc = create('<?xml version="1.0"?><title>&lt;img src=&quot;https://cdn.cloudflare.steamstatic.com&quot; /&gt;</title>').toObject();
        fixXmlbuilderObjectBug(doc);
        expect(doc).toEqual({
            title: '<img src="https://cdn.cloudflare.steamstatic.com" />',
        });
    });
    test('correctly transforms XML with HTML content to an object and back to XML', () => {
        const doc = create('<?xml version="1.0"?><title>&lt;img src=&quot;https://cdn.cloudflare.steamstatic.com&quot; /&gt;</title>').toObject();
        fixXmlbuilderObjectBug(doc);
        expect(create(doc).end()).toBe('<?xml version="1.0"?><title>&lt;img src="https://cdn.cloudflare.steamstatic.com" /&gt;</title>');
    });
});
