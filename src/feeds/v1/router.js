'use strict';
const express = require('express');
const router = express.Router();

const SUGGESTIONS = require('./suggestions.json');

router.get('/search', async function (req, res) {
    const { lang } = req.query;

    let feeds = SUGGESTIONS;

    if (lang) {
        feeds = feeds.filter(feed => feed.language === lang);
    }

    res.status(200)
        .json(feeds);
});

module.exports = router;
