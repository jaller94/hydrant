import { create as ipfsClient } from 'ipfs-http-client';

export default ipfsClient({
    apiPath: process.env.IPFS_API || 'http://localhost:5001'
});
