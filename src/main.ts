require('dotenv').config();
import log from './logger';
import app from './app';

async function main() {    
    const port = process.env.PORT || 3000;
    app.listen(port);
    log.info(`Listing on port ${port}`);
}

main().catch((error) => {
    log.error(error);
    process.exit(1);
});
