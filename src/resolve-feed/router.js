'use strict';
const express = require('express');
const superagent = require('superagent');
const log = require('../logger').default;
const {
    extractFeedUrlsFromHTML,
} = require('../v2/html');

const router = express.Router();

router.get('/', async function (req, res) {
    let { url } = req.query;
    if (typeof url !== 'string') {
        res.status(404).end('No url provided');
        return;
    }
    if (!/:\/\//.test(url)) {
        url = `https://${url}`;
    }

    let sourceReq;
    try {
        sourceReq = await superagent.get(url)
            .accept('text/html, application/rss+xml;q=0.9, application/atom+xml;q=0.8, application/rdf+xml;q=0.7, application/xml;q=0.6, text/xml;q=0.5, */*;q=0.1')
            .timeout({
                response: 4000,  // Wait 4 seconds for the server to start sending,
                deadline: 10000, // but allow 10 seconds for the file to finish loading.
            });
    } catch (error) {
        res.status(404).end('Request to url failed.');
        log.warn('resolve-feed: superagent failed', {error});
        return;
    }

    if (!sourceReq.ok) {
        res.status(404).end(`Host replied with HTTP error ${sourceReq.code}`);
        return;
    }

    const data = sourceReq.text || sourceReq.body.toString();
    const links = extractFeedUrlsFromHTML(data, url);
    if (links && links.length > 0) {
        res.end(links[0]);
    } else {
        res.status(404).end('No feeds found.');
    }
});

module.exports = router;
