'use strict';
const express = require('express');
const router = express.Router();
const superagent = require('superagent');
const { create } = require('xmlbuilder2');
const log = require('../logger').default;
const {
    fixXmlbuilderObjectBug,
} = require('../v2/helpers');

router.get('/', async function (req, res) {
    if (typeof req.query.source !== 'string' || req.query.source.trim() === '') {
        res.status(400).end('No source defined.');
        return;
    }
    if (!(req.query.source.startsWith('http://') || req.query.source.startsWith('https://'))) {
        res.status(400).end('Source must use the HTTP(S) protocol.');
        return;
    }
    const sourceReq = await superagent.get(req.query.source)
        .accept('application/rss+xml, application/atom+xml;q=0.9, application/rdf+xml;q=0.8, application/xml;q=0.7, text/xml;q=0.6, */*;q=0.1')
        .timeout({
            response: 4000,  // Wait 4 seconds for the server to start sending,
            deadline: 10000, // but allow 10 seconds for the file to finish loading.
        });

    if (!sourceReq.ok) {
        res.status(404).end('Failed to download source.');
        log.warn('Failed to download source.', { sourceReq });
        return;
    }

    let doc;
    try {
        doc = create(sourceReq.text).toObject();
        fixXmlbuilderObjectBug(doc);
    } catch (error) {
        log.warn(error);
        res.status(404).end('Failed to parse XML.');
        return;
    }
    if (!Array.isArray((((doc || {}).rss || {}).channel || {}).item)) {
        res.status(400).end('Unsupported input feed. Currently only RSS is supported.');
        log.warn('Unsupported input feed. Currently only RSS is supported.', {doc});
        return;
    }
    if (typeof req.query['title-startsWith'] === 'string' && req.query['title-startsWith']) {
        doc.rss.channel.item = doc.rss.channel.item.filter((item) => {
            return typeof item === 'object' && typeof item.title === 'string' && item.title.startsWith(req.query['title-startsWith']);
        });
    }
    if (typeof req.query['title-endsWith'] === 'string' && req.query['title-endsWith']) {
        doc.rss.channel.item = doc.rss.channel.item.filter((item) => {
            return typeof item === 'object' && typeof item.title === 'string' && item.title.endsWith(req.query['title-endsWith']);
        });
    }
    if (typeof req.query['title-contains'] === 'string' && req.query['title-contains']) {
        doc.rss.channel.item = doc.rss.channel.item.filter((item) => {
            return typeof item === 'object' && typeof item.title === 'string' && item.title.includes(req.query['title-contains']);
        });
    }
    if (typeof req.query['title-containsNot'] === 'string' && req.query['title-containsNot']) {
        doc.rss.channel.item = doc.rss.channel.item.filter((item) => {
            return typeof item === 'object' && typeof item.title === 'string' && !item.title.includes(req.query['title-containsNot']);
        });
    }
    if (typeof req.query.category === 'string' && req.query.category) {
        doc.rss.channel.item = doc.rss.channel.item.filter((item) => {
            return typeof item === 'object' && item.category === req.query.category;
        });
    }
    res.status(200)
        .header('Content-Type', 'application/rss+xml; charset=UTF-8')
        .end(
            create(doc).end({ prettyPrint: true })
        );
});

module.exports = router;
