'use strict';
const express = require('express');
const router = express.Router();
const fsPromises = require('fs').promises;
const path = require('path');
const log = require('../logger').default;

async function storeFeedback({ mood, text }) {
    if (typeof mood !== 'string') {
        throw Error('mood must be a string');
    }
    if (typeof text !== 'string') {
        throw Error('text must be a string');
    }
    const filename = path.join(process.env.DATABASE_FOLDER, `${Date.now()}.json`);
    const data = JSON.stringify({
        mood,
        text,   
    });
    await fsPromises.writeFile(filename, data, 'utf8');
}

router.post('/', async function (req, res) {
    try {
        await storeFeedback({
            ...req.body,
            timestamp: Date.now(),
        });
        res.status(200).end('Thank you so much for your feedback!');
    } catch (error) {
        log.warn(error);
        res.status(500).end('Failed to store feedback.');
    }
});

module.exports = router;
