'use strict';
const express = require('express');
const router = express.Router();
const {
    fixXmlbuilderObjectBug,
    getInput,
} = require('../v2/helpers');

router.get('/', async function (req, res) {
    if (typeof req.query.source !== 'string' || req.query.source.trim() === '') {
        res.render('view', {
            suggestedFeeds: [
                {
                    'title': 'BBC: Top Stories',
                    'url': 'http://feeds.bbci.co.uk/news/rss.xml',
                },
                {
                    'title': 'BBC: Science & Environment',
                    'url': 'http://feeds.bbci.co.uk/news/science_and_environment/rss.xml',
                },
                {
                    'title': 'Tagesschau',
                    'url': 'https://www.tagesschau.de/xml/rss2/',
                },
                {
                    'title': 'Hacker News',
                    'url': 'https://news.ycombinator.com/rss',
                },
            ],
        });
        return;
    }
    if (!(req.query.source.startsWith('http://') || req.query.source.startsWith('https://'))) {
        res.status(400).render('view', {
            errorMessage: 'Source must use the HTTP(S) protocol.',
        });
        return;
    }

    let doc;
    try {
        doc = await (await getInput(req.query.source)).toObject();
        fixXmlbuilderObjectBug(doc);
    } catch (error) {
        res.status(404).render('view', {
            errorMessage: 'Failed to process source.',
        });
        console.log('Failed to get source.');
        console.log(error);
        return;
    }

    if (!Array.isArray((((doc || {}).rss || {}).channel || {}).item)) {
        res.status(400).render('view', {
            errorMessage: 'Unsupported input feed. Currently only RSS is supported.',
        });
        console.warn('Unsupported input feed. Currently only RSS is supported.', doc);
        return;
    }
    res.render('view', {
        items: doc.rss.channel.item, 
    });
});

module.exports = router;
