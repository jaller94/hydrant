'use strict';
const express = require('express');
const router = express.Router();
const log = require('../logger').default;
const {
    getRSSForInstagram,
} = require('./script.js');

// Redirect for the instagram form
router.get('/', async function (req, res) {
    const { username } = req.query;
    if (typeof username !== 'string') {
        res.status(404).end('May I suggest to visit /instagram/nasa?');
        return;
    }
    res.redirect(`/instagram/${username}`);
});

router.get('/:user', async function (req, res) {
    const { user } = req.params;
    if (typeof user !== 'string') {
        res.status(400).end('User name must be a string');
        return;
    }
    if (user.length > 30) {
        res.status(400).end('User name must not exceed 30 characters');
        return;
    }
    if (!/^[A-Za-z0-9_.]+$/.test(user)) {
        res.status(400).end('User name contains characters forbidden in Instagram user names');
        return;
    }
    const options = {
        user,
        service: 'instagram',
        format: 'atom',
    };
    try {
        const xml = await getRSSForInstagram(options);
        res.status(200)
            .header('Content-Type', 'text/xml; charset=UTF-8')
            .end(xml);
    } catch (error) {
        log.warn(error);
        res.status(500).end('Nope');
    }
});

module.exports = router;
