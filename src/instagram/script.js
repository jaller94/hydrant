'use strict';
const fsPromises = require('fs').promises;
const path = require('path');
const { create } = require('xmlbuilder2');

const { spawn } = require('child_process');

function downloadInstagramInternal(user, folder) {
    return new Promise((resolve, reject) => {
        const ls = spawn(
            'instalooter',
            ['user', user, '--dump-json', '--get-videos', '--new'],
            {
                cwd: folder,
            }
        );
        ls.on('close', (code) => {
            if (code !== 0) {
                reject(`instalooter exited with code ${code}`);
            }
            resolve();
        });
        ls.on('error', () => {
            reject('Failed to start instalooter. Is it installed and in the PATH?');
        });
    });
}

async function downloadInstagramInt(user) {
    const folder = path.join(process.env.TMP_FOLDER, user);
    try {
        await fsPromises.mkdir(folder);
    } catch (error) {
        if (error.code !== 'EEXIST') {
            throw error;
        }
    }
    console.debug(`Instagram download start: ${user}`);
    await downloadInstagramInternal(user, folder);
    console.debug(`Instagram download ended: ${user}`);
    return folder;
}

const ongoingDownloads = new Map();

function downloadInstagram(user) {
    let promise = ongoingDownloads.get(user);
    if (!promise) {
        promise = downloadInstagramInt(user).then(() => {
            ongoingDownloads.delete(user);
        });
        ongoingDownloads.set(user, promise);
    }
    return promise;
}

function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}


function plain2Html(str) {
    if (typeof str !== 'string') {
        return;
    }
    const texts = str.split(/\n\n|\r\r/);
    let final = '';
    for (const part of texts) {
        final += `<p>${part.split(/\n|\r/).map(t => htmlEntities(t)).join('<br>')}</p>`;
    }
    return final;
}

function getImages(images, min, max) {
    const minNumber = typeof min === 'string' ? BigInt(min) : undefined;
    const maxNumber = typeof max === 'string' ? BigInt(max) : undefined;
    const arr = [];
    for (const image of images) {
        if (typeof image.name !== 'string') {
            continue;
        }
        let id;
        try {
            id = BigInt(image.name.replace(/\..+$/i, ''));
        } catch {
            //no-op
        }
        if (id <= maxNumber && (minNumber === undefined || id > minNumber)) {
            arr.push(image);
        }
    }
    return arr;
}



async function main2(name, folder) {
    let infos = [];
    const allImages = [];
    const allVideos = [];
    const files = await fsPromises.readdir(folder);
    for (const file of files) {
        if (/\.json$/.test(file)) {
            const filePath = path.join(folder, file);
            infos.push({
                ...JSON.parse(
                    await fsPromises.readFile(filePath, 'utf8'),
                ),
            });
        }
        if (/\.jpg$/.test(file)) {
            const filePath = path.join(folder, file);
            // const ipfsFile = await ipfs.add({
            //     content: await fsPromises.readFile(filePath),
            // });
            allImages.push({
                name: file,
                url: `${process.env.BASE_URL}/media/instagram/${file}`,
                //url: `https://ipfs.io/ipfs/${ipfsFile.path}`,
            });
            try {
                await fsPromises.copyFile(filePath, path.join(process.env.MEDIA_FOLDER, 'instagram', file));
            } catch (error) {
                if (error.code !== 'EEXIST') {
                    throw error;
                }
            }
        }
        if (/\.mp4$/.test(file)) {
            const filePath = path.join(folder, file);
            // const ipfsFile = await ipfs.add({
            //     content: await fsPromises.readFile(filePath),
            // });
            allVideos.push({
                name: file,
                url: `http://192.168.1.141:3000/media/instagram/${file}`,
                //url: `https://ipfs.io/ipfs/${ipfsFile.path}`
            });
            try {
                await fsPromises.copyFile(filePath, path.join(process.env.MEDIA_FOLDER, 'instagram', file));
            } catch (error) {
                if (error.code !== 'EEXIST') {
                    throw error;
                }
            }
        }
    }

    infos.sort((a, b) => {
        const aId = Number.parseInt(a.id);
        const bId = Number.parseInt(b.id);
        return aId - bId;
    });
    infos.reverse();

    const root = {
        feed: {
            '@xmlns': 'http://www.w3.org/2005/Atom',
            '@xmlns:media': 'http://search.yahoo.com/mrss/',
            id: `instagram:owner:${name}`,
            title: `${name} on Instagram`,
            updated: new Date().toISOString(),
            author: {
                name,
                uri: `https://instagram.com/${name}`,
            },
            entry: [],
        },
    };

    await Promise.all(infos.map(async (info, index, array) => {
        const episode = {
            id: `instagram:post:${info.id}`,
            title: `Post ${array.length - index}`,
        };
        root.feed.entry.push(episode);
        if (info.shortcode) {
            episode.link = {
                '@rel': 'alternate',
                '@href': `https://www.instagram.com/p/${info.shortcode}/`,
            };
        }
        const owner = info.owner || {};
        if (owner.full_name || owner.name) {
            const author = episode.author = {};
            if (owner.full_name) {
                author.name = owner.full_name;
            }
            if (owner.name) {
                author.uri = `https://instagram.com/${name}`;
            }
        }
        const plainText = ((((info.edge_media_to_caption || {}).edges || [])[0] || {}).node || {}).text;
        let text = '';
        const infoBefore = index + 1 <= array.length ? infos[index + 1] : undefined;
        const images = getImages(allImages, (infoBefore || {}).id, info.id);
        const videos = getImages(allVideos, (infoBefore || {}).id, info.id);
        for (const image of images) {
            text += `<div><img src="${image.url}"></div>`;
        }
        for (const video of videos) {
            text += `<div><video src="${video.url}">The post includes a video, but your browser doesn&apos;t support it.</video></div>`;
        }
        if (plainText) {
            text += plain2Html(plainText);
        }
        episode.content = {
            '@type': 'html',
            '$': text,
        };
        if (info.taken_at_timestamp) {
            episode.published = new Date(info.taken_at_timestamp * 1000).toISOString();
            episode.updated = new Date(info.taken_at_timestamp * 1000).toISOString();
        }
    }));

    return create(root).end({
        format: 'xml',
        prettyPrint: true,
    });
}

async function getRSSForInstagram(options) {
    const {format, user, service } = options;
    if (!format || !user || !service) {
        throw Error('required option missing');
    }
    // const cachePath = path.join(process.env.CACHE_FOLDER, `${service}_${format}_${user}.atom`);
    // const res = await ipfs.add(JSON.stringify(options), {
    //     onlyHash: true,
    // });
    // const cachePath = path.join(process.env.CACHE_FOLDER, `${res.cid}.atom`);
    // if (useCache) {
    //     try {
    //         return await fsPromises.readFile(cachePath, 'utf8');
    //     } catch (error) {
    //         // no-op
    //     }
    // }
    const folder = await downloadInstagram(user);
    const atom = await main2(user, folder);
    // fsPromises.writeFile(cachePath, atom, 'utf8');
    return atom;
}

module.exports = {
    getRSSForInstagram,
};
