'use strict';
const fsPromises = require('fs').promises;
const express = require('express');
const router = express.Router();
const {
    getLatestFeedbackContent,
    feedback2Feed,
} = require('./helpers');

router.use((req, res, next) => {
    if (!res.locals.permissions.includes('read_feedback')) {
        res.set('WWW-Authenticate', 'Basic realm="Restricted during development"');
        return res.status(401).send('Restricted during development');
    }
    next();
});

router.get('/', async function (req, res) {
    res.render('stats', {
        tmp: await fsPromises.readdir(process.env.DATABASE_FOLDER),
    });
});

router.get('/feed', async function (req, res) {
    const feedbackEntries = await getLatestFeedbackContent();
    const feed = feedback2Feed(feedbackEntries, new Date());
    return res.status(200).end(feed);
});

module.exports = router;
