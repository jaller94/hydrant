const fsPromises = require('fs').promises;
const path = require('path');
const { create } = require('xmlbuilder2');

/**
 * Gets the content of the most recently submitted feedback.
 * @param {number} limit
 * @returns {Promise<unknown[]>}
 */
async function getLatestFeedbackContent(limit = 50) {
    const files = await fsPromises.readdir(process.env.DATABASE_FOLDER);
    files.reverse();
    files.slice(0, limit);
    const feedbackEntries = await Promise.all(files.map(async (fileName) => {
        const filePath = path.join(process.env.DATABASE_FOLDER, fileName);
        const data = JSON.parse(await fsPromises.readFile(filePath, 'utf8'));
        return {
            timestamp: new Date(Number.parseInt(fileName.replace('.json', ''))).toISOString(),
            ...data,
        };
    }));
    return feedbackEntries;
}

function feedback2Feed(feedbackEntries, updatedDate) {
    const root = {
        feed: {
            '@xmlns': 'http://www.w3.org/2005/Atom',
            '@xmlns:media': 'http://search.yahoo.com/mrss/',
            id: `${process.env.BASE_URL}/read-feedback/feed`,
            title: 'Hydrant Feedback',
            updated: updatedDate.toISOString(),
            author: {
                name: 'Anonymous user',
            },
            entry: feedbackEntries.map(f => ({
                id: `${process.env.BASE_URL}/feedback/${f.timestamp}`,
                content: {
                    '@type': 'text',
                    '$': `${f.text}\n\nMood:${f.mood}`,
                },
                published: f.timestamp,
            })),
        },
    };
    return create(root).end({
        format: 'xml',
        prettyPrint: true,
    });
}

module.exports = {
    getLatestFeedbackContent,
    feedback2Feed,
};
