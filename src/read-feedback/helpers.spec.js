const {
    feedback2Feed,
} = require('./helpers');

process.env.BASE_URL = 'http://value-for-tests';

describe('feedback2Feed', () => {
    test('matches snapshot', () => {
        const feedbackEntries = [
            {
                mood: '5',
                timestamp: new Date(1605262215838).toISOString(),
                text: 'I really like your piece of software.',
            },
            {
                mood: '4',
                timestamp: new Date(1605263315838).toISOString(),
                text: 'I wish I could see my most recent searches in a sidebar.',
            },
        ];
        expect(feedback2Feed(feedbackEntries, new Date(1605363315838))).toMatchSnapshot();
    }); 
});
