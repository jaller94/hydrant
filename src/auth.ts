import auth from 'basic-auth';
import express from 'express';
import compare from 'tsscmp';

const check = (name: string, pass: string) => {
    if (!process.env.ADMIN_USERNAME || !process.env.ADMIN_PASSWORD) {
        return true;
    }
    let valid = true;   // Simple method to prevent short-circuit and use timing-safe compare   
    valid = compare(name, process.env.ADMIN_USERNAME) && valid;
    valid = compare(pass, process.env.ADMIN_PASSWORD) && valid;

    return valid;
};

const basicAuth = (req: express.Request, res: express.Response, next: any) => {
    const credentials = auth(req);
    res.locals.permissions = [];
    if (credentials && check(credentials.name, credentials.pass)) {
        res.locals.permissions.push('stats', 'read_feedback');
    }
    return next();
};

export default basicAuth;
