'use strict';
import path from 'path';
import express from 'express';
import auth from './auth';
const app = express();

import log from './logger';
const feedSuggestions = require('./feeds/v1/suggestions.json');

// Security:
// https://expressjs.com/en/advanced/best-practice-security.html#at-a-minimum-disable-x-powered-by-header
app.disable('x-powered-by');

app.use(auth);

app.all('*', (req, res, next) => {
    log.info('incoming request', {url: req.url});
    next();
});

const corsWildcard = (req: express.Request, res: express.Response, next: any) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    next();
};

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.raw());

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use('/v1', corsWildcard, require('./v1/router.js'));
app.use('/v2', corsWildcard, require('./v2/router.js'));
app.use('/feeds/v1', corsWildcard, require('./feeds/v1/router.js'));
app.use('/resolve-feed', corsWildcard, require('./resolve-feed/router.js'));
if (process.env.ENABLE_INSTAGRAM === 'true') {
    app.use('/instagram', corsWildcard, require('./instagram/router.js'));
}

app.use('/feedback', require('./feedback/router.js'));
app.use('/read-feedback', require('./read-feedback/router.js'));

// app.use('/media', express.static(process.env.MEDIA_FOLDER));

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', (req, res) => {
    res.render('index', {
        BASE_URL: process.env.BASE_URL,
        feedSuggestions,
    });
});

app.get('/about', (req, res) => {
    res.render('about');
});

app.get('/docs', (req, res) => {
    res.render('docs');
});

app.use('/view', require('./view/router.js'));

app.get('/stats', async (req, res) => {
    if (!res.locals.permissions.includes('stats')) {
        res.set('WWW-Authenticate', 'Basic realm="Restricted during development"');
        return res.status(401).send('Restricted during development');
    }
    const fsPromises = require('fs').promises;
    res.render('stats', {
        tmp: await fsPromises.readdir(process.env.TMP_FOLDER),
    });
});

export default app;
