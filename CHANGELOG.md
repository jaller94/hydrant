=== 2020-12-30 ===
* Sources referencing an HTML document, get resolved to a feed Url.
* Feed URLs without a schema are assumed to be https resources.
* Added feed suggestions.
* The production environment can now be deployed through GitLab. #infra
* The production environment now uses docker. #infra
