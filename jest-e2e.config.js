module.exports = {
    modulePathIgnorePatterns: [
        '<rootDir>/dist/',
    ],
    testRegex: '(/__e2e__/.*)\\.[jt]sx?$',
};
