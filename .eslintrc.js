module.exports = {
    'env': {
        'commonjs': true,
        'node': true,
        'es2021': true,
    },
    'extends': 'eslint:recommended',
    'parserOptions': {
        'ecmaVersion': 12,
    },
    'rules': {
        'indent': [
            'error',
            4,
            {
                'SwitchCase': 1,
            },
        ],
        'linebreak-style': [
            'error',
            'unix',
        ],
        'quotes': [
            'error',
            'single',
        ],
        'semi': [
            'error',
            'always',
        ],
    },
    overrides: [
        {
            files: [
                '*.spec.js',
                '**/__tests__/*',
                '**/__e2e__/*'
            ],
            extends: ['plugin:jest/recommended']
        },
    ]
};
