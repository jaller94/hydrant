# Hydrant

Don't let others dictate what you see and miss online – decide yourself!

Hydrant gives you control over the news you consume. The software aggregates news, transforms and filters it. You can point your RSS Feed Reader to it and receive just the news what you have asked for.

## Configuration
Copy the file `.env-sample` to `.env` and configure the server.

To run the application you need to have NodeJS installed.

Then run
```
npm install --production
npm run start
```
