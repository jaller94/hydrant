# Hydrant
## Projektbeschreibung (max. 700 Zeichen)

Hydrant ist eine Webserver-Anwendung zum Filtern, Ergänzen, Organisieren von RSS-Feeds. Im größeren Kontext soll es journalistische Arbeit durch Souveränität der eigenen Nachrichten-Feeds unterstüzen.

Hydrant liest das RSS-Dateiformat welches von Blogs, Podcasts und manchen sozialen Medien verwendet wird. Durch Services wie RSS.app werden auch Dienste wie Twitter und Instagram unterstützt, bei denen keine eigene Schnittstelle vorhanden ist. Damit können Nutzer:innen mit Hydrant den Großteil von Nachrichtenströmen im Internet im Blick behalten.

## Gesellschaftliche Herausforderungen (max. 1300)

RSS bietet Nutzer:innen verschiedene Vorteile über die Nutzung von Webseiten und sozialen Medien. Zur journalistischen Arbeit können verschiedene Nachrichtenquellen aggregiert und gefiltert werden.

Hydrant soll es erlauben einem Thema oder einer Person auf mehreren Platformen zu folgen. Redundante Artikel (Cross posting) würden zu einem Artikel zusammengefasst.

Durch ein Filtern von Artikelinhalten entfallen unerwünschte Themen und Werbung. Hydrant wird damit einen besseren Fokus erlauben und reduziert die Suchtgefahr beim Lesen.

Durch das Cachen von Feeds und Medien kann Hydrant die Privatsspäre seiner Nutzer:innen verbessern. Dies ist besonders wichtig für die Sensibilität bei einer journalistischen Recherche. IP-Adressen und Abrufzeiten von Nutzer:innen werden durch die Hydrant-Instanz anonymisiert.

Als Aggregations-Platform für viele verschiedene Rohdaten kann Hydrant helfen aus Filterblasen auszubrechen. Es soll ermöglicht werden mehr Quellen einfach zu folgen, als dies mit aktueller Software möglich ist. Nutzer:innen behalten so die Souveränität über den eigenen Fokus und beseitigen den Bias von personalisierten Webseiten wie Social-Media-Feeds.

## Technische Umsetzung (max. 1300 Zeichen)

Hydrant baut auf Node.js und den Express-Webserver. Express ist ein schneller und flexibler Webserver mit weiter Verbreitung.

Wo immer möglich soll JavaScript im Front-End optional bleiben, um eine hohe Kompatilität zu verschiedensten Endgeräten zu gewähren. So sollen auch Nutzer:innen ohne JavaScript, mit veralteter Software und speziellen Webbrowsern (z.B. Smart-TVs) Hydrant nutzen können.

Der Fokus auf HTTP(S), HTML und RSS/Atom soll eine Langlebigkeit der Software und seiner API garantieren, welche durch JavaScript und andere Technologien auf der Client-Seite möglicherweise reduziert würde.

Um möglichst viele Feed-Reader abzudecken soll Hydrant die Ausgabeformate RSS1, RSS2 und Atom unterstützen. Langfristig ist die serverseitige Konvertierung zu anderen Formaten möglich, z.B. ActivityPub und Matrix.

## Beschreibung des aktuellen Stands und geplante Neuerungen (max. 700 Zeichen)

Ein Prototyp des technischen Grundgerüsts ist da. Es fehlen wichtige Tests um die Stabilität der Anwendung zu garantieren.

Ein grundsätzliches Filtern von Artikeln auf der Basis von Überschriften und Kategorien im Feed ist möglich. Funktionen zum Filtern und Ergänzen von Artikelinhalten fehlen.

Für die Verarbeitung von Feeds soll ein modulares System geschrieben werden. Durch wiederverwendbare Bausteine werden sich so Filter-, Aggregations- und Sortier-Funktionen aneinanderreihen lassen.Nutzer:innen sollen mit diesen Bausteinen die Möglichkeit bekommen selbst neue, komplexere Verarbeitungen zu erstellen. Die Grundbausteine werden von mir in JavaScript geschrieben und die Baupläne würden vermutlich in einem JSON-Format abgespeichert, welches ich mit JSON-Schema standardisiere.

Momentan ist nur RSS2 unterstützt und die Software kann nicht mit invaliden Feeds umgehen. Atom und RSS1 sollen als Eingabe- und Ausgabeformat unterstützt werden. Des Weiteren soll das Einlesen von Feeds mit kleinen Syntaxfehlern möglich sein. Klares Ziel ist es den Großteil von Feeds im Internet abzudecken.

## Link zum bestehenden Projekt

https://hydrant.chrpaul.de

## Ähnliche Ansätze und was dieses Projekt besser macht (max. 400 Zeichen)

* Yahoo! Pipes – Nicht quelloffen und eingestellt in 2015
* Huginn – Open-source Server für Eingabe- und Ausgabeströme verschiedenster APIs. Nur Hydrant hat einen Fokus auf Nachrichten-Feeds.
* Node RED – Open-source Automation basierend auf Datenströmen. RSS ist kein besonderer Fokus in diesem Projekt.
* OPML – Dateiformat zum Austausch von RSS-URLs. Hydrant könnte dynamisch Feeds über mehrere Geräte verwalten.

## Wer ist die Zielgruppe und wie erreicht das Projekt sie (max. 700 Zeichen)

Die Software richtet sich an Menschen, die ihre Nachrichten online konsumieren und eine große Selbstkontrolle über die Quellen und Inhalte bevorzugen. Eine besondere Zielgruppe sind Journalisten und solche die für Recherchen aus ihrer Filterblase ausbrechen wollen.

## Wichtigste Meilensteine im Förderzeitraum (max. 700 Zeichen)
